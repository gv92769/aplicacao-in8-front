import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Candidato } from '../candidato';

@Injectable({
  providedIn: 'root'
})
export class CandidatoService {

  urlSalvar = "http://127.0.0.1:8000/api/in8/candidato/salvar";

  urlListAll = "http://127.0.0.1:8000/api/in8/candidato/listAll";

  constructor(private httpClient: HttpClient) { }

  salvar(candidato: Candidato): Observable<Boolean> {
    return this.httpClient.post<Boolean>(this.urlSalvar, candidato);
  }

  listar(): Observable<Candidato[]> {
    return this.httpClient.get<Candidato[]>(this.urlListAll);
  }

}
