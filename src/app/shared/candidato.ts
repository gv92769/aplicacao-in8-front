export class Candidato {
  id: number;
  nome: String;
  email: String;
  nascimento: Date;
  telefone: String;
}
