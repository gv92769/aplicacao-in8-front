import { Component, OnInit } from '@angular/core';
import { Candidato } from '../shared/candidato';
import { CandidatoService } from '../shared/service/candidato.service';

@Component({
  selector: 'app-lista',
  templateUrl: './lista.component.html',
  styleUrls: ['./lista.component.css']
})
export class ListaComponent implements OnInit {

  cols: any[];

  colsM: any[];

  candidatos: Candidato[];

  constructor(private candidatoService: CandidatoService) { }

  ngOnInit(): void {
    this.cols = [
      { field: 'nome', header: 'NOME' },
      { field: 'email', header: 'E-MAIL' },
      { field: 'nascimento', header: 'NASCIMENTO' },
      { field: 'telefone', header: 'TELEFONE' },
    ];

    this.colsM = [
      { field: 'nome', header: 'Nome' },
      { field: 'email', header: 'E-mail' },
      { field: 'nascimento', header: 'NASC.' },
      { field: 'telefone', header: 'TEL.' },
    ];

    this.listar();
  }

  listar() {
    this.candidatoService.listar().subscribe(resp => this.candidatos = resp);
  }

}
