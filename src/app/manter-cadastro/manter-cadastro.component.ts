import { Component, OnInit } from '@angular/core';
import { Candidato } from '../shared/candidato';
import { CandidatoService } from '../shared/service/candidato.service';
import { ListaComponent } from '../lista/lista.component';
import { Validators, FormControl, FormGroup, FormBuilder, Form } from '@angular/forms';

@Component({
  selector: 'app-manter-cadastro',
  templateUrl: './manter-cadastro.component.html',
  styleUrls: ['./manter-cadastro.component.css']
})
export class ManterCadastroComponent implements OnInit {

  candidato: Candidato = new Candidato();

  candidatoForm: FormGroup;

  constructor(private candidatoService: CandidatoService, private listaComponent: ListaComponent, private fb: FormBuilder) { }

  ngOnInit(): void {
    this.candidatoForm = this.fb.group({
      'nome': new FormControl('', Validators.compose([Validators.required])),
      'email': new FormControl('', Validators.compose([Validators.required, Validators.pattern(/^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/)])),
      'nascimento': new FormControl('', Validators.compose([Validators.required])),
      'telefone': new FormControl('', Validators.compose([Validators.required, Validators.pattern(/^(\(?\d{2}\)?\s)?(\d{4,5}\-\d{4}) || (\(?\d{2}\)?\s)\?d{1}?\s(\d{4}\-\d{4})/)])),
    });
  }

  salvar() {
    if (this.candidatoForm.valid) {
      this.candidatoService.salvar(this.candidato).subscribe(resp => {
        if (resp == true) {
          this.listaComponent.listar();
        }
      });
    }
  }
}
