import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ManterCadastroComponent } from './manter-cadastro.component';

describe('ManterCadastroComponent', () => {
  let component: ManterCadastroComponent;
  let fixture: ComponentFixture<ManterCadastroComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ManterCadastroComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManterCadastroComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
